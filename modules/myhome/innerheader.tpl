<div class="container">
    {if $isHome}
        <div class="row">
            <div class="col-sm-12">
                <div class="container_home2">
                    <div class="contenido home">
                        <div class="row">
                            <div class="col-md-6">&nbsp;</div>
                            <div class="col-md-6"><img src="{$base_dir}themes/kelinda/imagenes/logo_blanco.png" class="text-center" /></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <ul class="menu home">
                                    <li><a href="{$base_dir}">Inicio</a></li>
                                    <li><a href="{$base_dir}index.php?controller=custom&tarea=cathome&id_cat=3">Mujeres</a></li>
                                    <li><a href="{$base_dir}index.php?controller=custom&tarea=cathome&id_cat=45">Hombres</a></li>
                                    <li><a href="{$base_dir}index.php?controller=custom&tarea=cathome&id_cat=47">Ni&ntilde;as</a></li>
                                    <li><a href="{$base_dir}index.php?controller=custom&tarea=cathome&id_cat=46">Ni&ntilde;os</a></li>
                                    <li><a href="{$base_dir}index.php?controller=custom&tarea=catalogo">Catalogo</a></li>
                                    <li><a href="{$base_dir}index.php?controller=custom&tarea=tiendas">Tiendas</a></li>
                                    <li><a href="{$base_dir}index.php?controller=custom&tarea=mostrarContacto">Contacto</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">&nbsp;</div>
                            <div class="col-md-4 contenido-home2">
                                <!--div>Enterizo de copa estructurada con aro,
                                control de abdomen, cortes que estilizan
                                la silueta, tiras graduables y espalda escotada</div>
                                <br />
                                <div><a href="http://andresmesa.co/kelinda/12-Array" class="boton carrito text-center"><img src="{$base_dir}themes/kelinda/imagenes/icono_carrito_black.png" /> &iexcl;Comprar ya!</a></div-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {else}
        <div class="row">
            <div class="col-md-3 col-lg-2">
                <a href="{$base_dir}">
                    <img src="{$base_dir}themes/kelinda/imagenes/logo.png" class="img-responsive" />
                </a>
            </div>
            <div class="col-md-9 col-lg-10">
                <div class="row">
                    <div class="col-md-12">
                        <img src="{$base_dir}themes/kelinda/imagenes/kelinda_nueva_coleccion.jpg" class="img-responsive img-fullw" />
                    </div>
                </div>
                <ul class="row-fluid top menu">
                    <li class="col-md-4 text-center">
                        <a href="{$link->getPageLink("order", true)|escape:"html":"UTF-8"}" class="boton carrito"><img src="{$base_dir}themes/kelinda/imagenes/icono_carrito_black.png" /> Mi carrito de compras</a>
                    </li>
                    <li class="col-md-4">
                        <img src="{$base_dir}themes/kelinda/imagenes/icono_kelinda_black.png" />
                        <div class="usuario top menu">
                            {if $logged}
                                <a href="{$link->getPageLink('my-account', true)|escape:'html'}" title="Mi cuenta" class="account left" rel="nofollow"><span>{$cookie->customer_firstname} {$cookie->customer_lastname}</span></a>
                                <span class="left">&nbsp;-&nbsp;</span>
                                <a href="{$link->getPageLink('index', true, NULL, "mylogout")|escape:'html'}" title="Logout" rel="nofollow" class="left">Logout</a>
                            {else}
                                <a class="login" href="{$link->getPageLink('my-account', true)|escape:'html'}" rel="nofollow" title="Login">Login</a>
                            {/if}
                        </div>
                    </li>
                    <li class="col-md-2">
                        <a href="{$link->getModuleLink('blockwishlist', 'mywishlist', array(), true)|addslashes}" title="Favoritos" rel="nofollow">
                            <img src="{$base_dir}themes/kelinda/imagenes/icono_favoritos.png" />
                            &nbsp;Favoritos
                        </a>
                    </li>
                    <li class="col-md-2">
                        <img src="{$base_dir}themes/kelinda/imagenes/icono_menu.png" />
                        &nbsp;Menu
                        <ul class="menu header">
                            {foreach from=$categorias item=cat}
                                <li>
                                    <a href="{$link->getCategoryLink($cat.id_category, $cat.link_rewrite)|escape:'html':'UTF-8'}">
                                        {$cat.name|escape:'html':'UTF-8'}
                                    </a>
                                </li>
                            {/foreach}
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    {/if}
</div>

