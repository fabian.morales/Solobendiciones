{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!-- Modulo home -->
<div id="seccion_banner_home" style="width: 100%; display: block">
    <ul id="banner_home">
        <li><img class="img-responsive img-fullw" src="{$base_dir}themes/saludecco/imagenes/banner1.jpg" /></li>
        <li><img class="img-responsive img-fullw" src="{$base_dir}themes/saludecco/imagenes/banner2.jpg" /></li>
        <li><img class="img-responsive img-fullw" src="{$base_dir}themes/saludecco/imagenes/banner3.jpg" /></li>
    </ul>
</div>

<div class="seccion-beneficios">
    <div class="container">
        <div class="row">
            <div class="col-md-3 text-center">
                <img class="img-responsive" src="{$base_dir}themes/saludecco/imagenes/esfera_mundo.png" />
                <p>Homeopatía de marcas líderes</p>
            </div>
            <div class="col-md-3 text-center">
                <img class="img-responsive" src="{$base_dir}themes/saludecco/imagenes/esfera_check.png" />
                <p>Cosméticos orgánicos certificados</p>
            </div>
            <div class="col-md-3 text-center">
                <img class="img-responsive" src="{$base_dir}themes/saludecco/imagenes/esfera_pin.png" />
                <p>Disponible en las principales ciudades</p>
            </div>
            <div class="col-md-3 text-center">
                <img class="img-responsive" src="{$base_dir}themes/saludecco/imagenes/esfera_eq.png" />
                <p>La mejor relación calidad-precio</p>
            </div>
        </div>
    </div>
</div>
<div class="seccion-productos-home">
    <div class="container-fluid">
        <ul id="slider_productos">
            {*<li><img class="img-responsive" src="{$base_dir}themes/saludecco/imagenes/producto_01.jpg" /></li>
            <li><img class="img-responsive" src="{$base_dir}themes/saludecco/imagenes/producto02.jpg" /></li>
            <li><img class="img-responsive" src="{$base_dir}themes/saludecco/imagenes/producto_03.jpg" /></li>
            <li><img class="img-responsive" src="{$base_dir}themes/saludecco/imagenes/producto_04.jpg" /></li>
            <li><img class="img-responsive" src="{$base_dir}themes/saludecco/imagenes/producto_05.jpg" /></li>*}
            {foreach from=$destacados item=product name=products}
                <li>
                    <a href="{$product.link|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="url">
                        <img class="img-responsive" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" title="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" {if isset($homeSize)} width="{$homeSize.width}" height="{$homeSize.height}"{/if} itemprop="image" />
                    </a>
                </li>
            {/foreach}
        </ul>
    </div>
</div>
<div class="seccion-atencion">
    <div class="container">
        <div class="row text-center-small">
            <div class="col-md-4 col-md-offset-2">
                <img src="{$base_dir}themes/saludecco/imagenes/telefono.png" />
                <span>Atención al cliente <br />
                    PBX: (1) 631-6975 <br /> Cel.: 316 522 75 94
                </span>
                <br />
            </div>
            <div class="col-md-4 col-md-offset-2">
                <img src="{$base_dir}themes/saludecco/imagenes/pin.png" />
                <a href="{$base_dir}index.php?controller=custom&tarea=tiendas" id="lnkTiendas">Conoce nuestras <br />
                    Farmacias aliadas
                </a>
                <br />
            </div>
        </div>
    </div>
</div>
<div class="seccion-marcas">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="titulo cyan">Marcas</div>
                <ul id="slider_marcas">
                    <li><a href="{$link->getCategoryLink(15)|escape:'html':'UTF-8'}"><img src="{$base_dir}themes/saludecco/imagenes/heel.png" /></a></li>
                    <li><a href="{$link->getCategoryLink(17)|escape:'html':'UTF-8'}"><img src="{$base_dir}themes/saludecco/imagenes/sobio.png" /></a></li>
                    <li><a href="{$link->getCategoryLink(16)|escape:'html':'UTF-8'}"><img src="{$base_dir}themes/saludecco/imagenes/solgar.png" /></a></li>
                    <li><a href="{$link->getCategoryLink(15)|escape:'html':'UTF-8'}"><img src="{$base_dir}themes/saludecco/imagenes/heel.png" /></a></li>
                    <li><a href="{$link->getCategoryLink(17)|escape:'html':'UTF-8'}"><img src="{$base_dir}themes/saludecco/imagenes/sobio.png" /></a></li>
                    <li><a href="{$link->getCategoryLink(16)|escape:'html':'UTF-8'}"><img src="{$base_dir}themes/saludecco/imagenes/solgar.png" /></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="separador verde">&nbsp;</div>
<div class="seccion-info-home text-center-small">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <a href="{$link->getCMSLink(3)|escape:'html'}">
                    <img class="img-responsive" src="{$base_dir}themes/saludecco/imagenes/formas_pago.png" />
                </a>
            </div>
            <div class="col-md-5">
                <a href="{$base_dir}blog.html">
                    <img class="img-responsive" src="{$base_dir}themes/saludecco/imagenes/consejos.png" />
                </a>
            </div>
            <div class="col-md-4">
                <a href="{$link->getCategoryLink(17)|escape:'html'}">
                    <img class="img-responsive" src="{$base_dir}themes/saludecco/imagenes/productos_nuevos.png" />
                </a>
            </div>
        </div>
    </div>
</div>

