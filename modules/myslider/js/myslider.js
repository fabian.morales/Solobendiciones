/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

$(document).ready(function(){

	if (typeof(myslider_speed) == 'undefined')
		myslider_speed = 500;
	if (typeof(myslider_pause) == 'undefined')
		myslider_pause = 3000;
	if (typeof(myslider_loop) == 'undefined')
		myslider_loop = true;
    if (typeof(myslider_width) == 'undefined')
        myslider_width = 779;


	if (!!$.prototype.bxSlider)
		$('#myslider').bxSlider({
			useCSS: false,
			maxSlides: 1,
			slideWidth: myslider_width,
			infiniteLoop: myslider_loop,
			hideControlOnEnd: true,
			pager: false,
			autoHover: true,
			auto: myslider_loop,
			speed: parseInt(myslider_speed),
			pause: myslider_pause,
			controls: true
		});

    $('.myslider-description').click(function () {
        window.location.href = $(this).prev('a').prop('href');
    });

	if ($('#htmlcontent_top').length > 0)
		$('#homepage-slider').addClass('col-xs-8');
	else
		$('#homepage-slider').addClass('col-xs-12');
});