<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="menu_hrz clearfix">
                <div id="despl-resp" class="despl-resp hidden-lg hidden-md">
                    <a href="#menu_horz">Menu</a>
                </div>  

                <ul id="menu_horz" class="clearfix menu-resp">
                    <li>
                        <a href="{$base_dir}index.php" title="Inicio">Comprar por marca</a>
                        <ul>
                            {foreach from=$categorias item=cat}
                            <li>
                                <a href="{$link->getCategoryLink($cat.id_category, $cat.link_rewrite)|escape:'html':'UTF-8'}">
                                    {$cat.name|escape:'html':'UTF-8'}
                                </a>
                            </li>
                            {/foreach}
                        </ul>
                    </li>
                    <li>
                        <a href="{$base_dir}index.php">Comprar por producto</a>
                        <ul>
                            {foreach from=$marcas item=m}
                            <li>
                                <a href="{$base_dir}index.php?controller=custom&tarea=marcas&id_marca={$m.id_supplier}">
                                    {$m.name|escape:'html':'UTF-8'}
                                </a>
                            </li>
                            {/foreach}
                        </ul>
                    </li>
                    <li class="redes">
                        Síguenos en
                        <a class="redes" href="https://www.facebook.com/solobendicionesoficial" target="_blank"><img src="{$base_dir}themes/saludecco/imagenes/facebook.png" /></a>
                        {*<a class="redes" href="#"><img src="{$base_dir}themes/saludecco/imagenes/pinterest.png" /></a>*}
                        <a class="redes" href="https://www.instagram.com/solobendicionesoficial/" target="_blank"><img src="{$base_dir}themes/saludecco/imagenes/instagram.png" /></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

