{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!-- Modulo sub-footer -->
{if $page_name == 'index'}
    {*<footer id="footer">
        <div class="container">
            <div class="row">
                
            </div>
        </div>
    </footer>*}
{else}
    
{/if}
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3 text-center lateral">
                Con el respaldo de:<br /><br />
                <img src="{$base_dir}themes/saludecco/imagenes/appsco.png" />
                <img src="{$base_dir}themes/saludecco/imagenes/mintic.png" />
                <img src="{$base_dir}themes/saludecco/imagenes/slogan_pais.png" />
            </div>
            <div class="col-md-3 text-center lateral">
                <div class="titulo">Marcas</div>
                <nav>
                    <ul>
                        {foreach from=$categorias item=cat}
                        <li>
                            <a href="{$link->getCategoryLink($cat.id_category, $cat.link_rewrite)|escape:'html':'UTF-8'}">
                                {$cat.name|escape:'html':'UTF-8'}
                            </a>
                        </li>
                        {/foreach}
                    </ul>
                </nav>
            </div>
            <div class="col-md-3 text-center lateral">
                <div class="titulo">Productos</div>
                <nav>
                    <ul>
                        {foreach from=$marcas item=m}
                        <li>
                            <a href="{$base_dir}index.php?controller=custom&tarea=marcas&id_marca={$m.id_supplier}">
                                {$m.name|escape:'html':'UTF-8'}
                            </a>
                        </li>
                        {/foreach}
                        <li>Y muchos m&aacute;s</li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-3 text-center-small">
                <nav class="cyan">
                    <ul>
                        <li><a href="{$link->getCMSLink(4)|escape:'html'}">Quiénes somos</a></li>
                        <li><a href="{$link->getCMSLink(5)|escape:'html'}">Preguntas y respuestas</a></li>
                        <li><a href="{$link->getCMSLink(2)|escape:'html'}">Políticas de privacidad</a></li>
                    </ul>
                </nav>
                <br />
                <span>Síguenos en</span>
                <a class="redes" href="https://www.facebook.com/solobendicionesoficial" target="_blank"><img src="{$base_dir}themes/saludecco/imagenes/facebook.png" /></a>
                {*<a class="redes" href="#"><img src="{$base_dir}themes/saludecco/imagenes/pinterest.png" /></a>*}
                <a class="redes" href="https://www.instagram.com/solobendicionesoficial/" target="_blank"><img src="{$base_dir}themes/saludecco/imagenes/instagram.png" /></a>
            </div>
        </div>
    </div>
    <div class="copy">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    2016 &copy; Solo bendiciones, Todos los derechos reservados
                </div>
            </div>
        </div>
    </div>
</footer>