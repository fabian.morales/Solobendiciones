<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_CAN_LOAD_FILES_'))
    exit;

class MyTCC extends CarrierModule
{
    const PREFIX = 'my_tcc_';
 
    protected $_hooks = array(
        'actionCarrierUpdate',
    );
 
    protected $_carriers = array(
        'TCC' => 'TCC',
    );
    public function __construct()
    {
	$this->name = 'mytcc';
	$this->tab = 'shipping_logistics';
	$this->version = '1.0.0';
	$this->author = 'Fabian Morales';
	$this->bootstrap = TRUE;
 
	parent::__construct();
 
	$this->displayName = $this->l('Transporte TCC');
	$this->description = $this->l('Transporte TCC');
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    public function install()
    {
	if (parent::install()) {
            foreach ($this->_hooks as $hook) {
                if (!$this->registerHook($hook)) {
                    return FALSE;
                }
            }

            if (!$this->createCarriers()) {
                return FALSE;
            }

            return TRUE;
	}
        
	return FALSE;
    }
 
    public function uninstall()
    {
	if (parent::uninstall()) {
            foreach ($this->_hooks as $hook) {
                if (!$this->unregisterHook($hook)) {
                    return FALSE;
                }
            }

            if (!$this->deleteCarriers()) {
                return FALSE;
            }

            return TRUE;
	}
 
	return FALSE;
    }

    protected function createCarriers()
    {
	foreach ($this->_carriers as $key => $value) {
            $carrier = new Carrier();
            $carrier->name = $key;
            $carrier->active = TRUE;
            $carrier->deleted = 0;
            $carrier->shipping_handling = FALSE;
            $carrier->range_behavior = 0;
            $carrier->delay[Configuration::get('PS_LANG_DEFAULT')] = $key;
            $carrier->shipping_external = TRUE;
            $carrier->is_module = TRUE;
            $carrier->external_module_name = $this->name;
            $carrier->need_range = TRUE;

            if ($carrier->add()) {
                $groups = Group::getGroups(true);
                foreach ($groups as $group) {
                    Db::getInstance()->autoExecute(_DB_PREFIX_ . 'carrier_group', array(
                        'id_carrier' => (int) $carrier->id,
                        'id_group' => (int) $group['id_group']
                    ), 'INSERT');
                }

                $rangePrice = new RangePrice();
                $rangePrice->id_carrier = $carrier->id;
                $rangePrice->delimiter1 = '0';
                $rangePrice->delimiter2 = '1000000';
                $rangePrice->add();

                $rangeWeight = new RangeWeight();
                $rangeWeight->id_carrier = $carrier->id;
                $rangeWeight->delimiter1 = '0';
                $rangeWeight->delimiter2 = '1000000';
                $rangeWeight->add();

                $zones = Zone::getZones(true);
                foreach ($zones as $z) {
                    Db::getInstance()->autoExecute(_DB_PREFIX_ . 'carrier_zone',
                        array('id_carrier' => (int) $carrier->id, 'id_zone' => (int) $z['id_zone']), 'INSERT');
                    Db::getInstance()->autoExecuteWithNullValues(_DB_PREFIX_ . 'delivery',
                        array('id_carrier' => $carrier->id, 'id_range_price' => (int) $rangePrice->id, 'id_range_weight' => NULL, 'id_zone' => (int) $z['id_zone'], 'price' => '0'), 'INSERT');
                    Db::getInstance()->autoExecuteWithNullValues(_DB_PREFIX_ . 'delivery',
                        array('id_carrier' => $carrier->id, 'id_range_price' => NULL, 'id_range_weight' => (int) $rangeWeight->id, 'id_zone' => (int) $z['id_zone'], 'price' => '0'), 'INSERT');
                }

                copy(dirname(__FILE__) . 'logo-coordinadora', _PS_SHIP_IMG_DIR_ . '/' . (int) $carrier->id . '.png'); //assign carrier logo

                Configuration::updateValue(self::PREFIX . $value, $carrier->id);
                Configuration::updateValue(self::PREFIX . $value . '_reference', $carrier->id);
            }
	}
 
	return TRUE;
    }
    
    protected function deleteCarriers()
    {
	foreach ($this->_carriers as $value) {
            $tmp_carrier_id = Configuration::get(self::PREFIX . $value);
            $carrier = new Carrier($tmp_carrier_id);
            $carrier->delete();
	}
 
	return TRUE;
    }
    
    public function getOrderShippingCost($params, $shipping_cost)
    {
        ini_set('display_errors', 1);
        $ret = 0;
        $cart = $this->context->cart;
        $valor = $cart->getOrderTotal(true, Cart::BOTH_WITHOUT_SHIPPING);
        
        if ($valor > 0){
            $origen = "11001000";
            $destino = "11001000";
        
            $dirEnvio = new Address(intval($cart->id_address_delivery));
            
            if (sizeof($dirEnvio) && !empty($dirEnvio->city)){
                list($destino, $nombreCiudad) = explode('-', $dirEnvio->city);
            }
            
            if ((int) $destino === 0) {
                $destino = "11001000";
            }
            
            $cart = $this->context->cart;
            $peso = $cart->getTotalWeight();
            
            if ($peso < 1){
                $peso = 1;
            }
            
            $cookie = new Cookie('frontier'); //make your own cookie
            $cookie->setExpire(time() + 120 * 60); // 20 minutes for example                        
            
            $key = $origen."-".$destino."-".$valor."-".$peso;
            $cache = $cookie->liq_transp;
            
            if (empty($cache)){
                $cache = array(
                    "key" => "",
                    "valor" => 0
                );
            }
            else{
                $cache = (array)json_decode($cache);
            }
            
            if ($key === $cache["key"] && false){
                $ret = floatval($cache["valor"]);
            }
            else{
                $url = "http://clientes.tcc.com.co/servicios/LiquidacionAcuerdos.asmx?wsdl";
                $cliente = new SoapClient($url, array("trace" => 1, "exception" => 0));

                $clave = "MEDECOFLORA";
                
                $liq = new stdClass();
                $liq->tipoenvio = "2";                
                $liq->idciudadorigen = "2";
                $liq->idciudaddestino = "3";
                $liq->valormercancia = $valor;
                $liq->boomerang = "0";
                $liq->cuenta = "5124500";
                $liq->fecharemesa = date('Y-m-d');
                $liq->idunidadestrategicanegocio = "2";

                $und = new stdClass();
                $und->numerounidades = "1";
                $und->pesoreal = str_replace(",", ".", round($peso, 1));
                $und->pesovolumen = 0;
                $und->alto = "0";
                $und->ancho = "0";
                $und->largo = "0";
                $und->tipoempaque = "";
                
                $liq->unidades = array($und);

                $params = array("Clave" => $clave, "Liquidacion" => $liq);
                $res = $cliente->consultarliquidacion($params);
                
                /*echo $cliente->__getLastRequest(); 
                echo $cliente->__getLastResponse(); 
                print_r($res);
                die();
                /*print_r($res);
                print_r($liq);*/

                if (sizeof($res)){
                    $ret = $res->consultarliquidacionResult->total->totaldespacho;
                    $cache = array(
                        "key" => $key,
                        "valor" => $ret
                    );
                    
                    $cookie->liq_transp = json_encode($cache);
                    $cookie->write();
                }
            }
        }
        
	return $ret;
    }
 
    public function getOrderShippingCostExternal($params)
    {
	return $this->getOrderShippingCost($params, 0);
    }
    
    public function hookActionCarrierUpdate($params)
    {
	if ($params['carrier']->id_reference == Configuration::get(self::PREFIX . 'swipbox_reference')) {
            Configuration::updateValue(self::PREFIX . 'swipbox', $params['carrier']->id);
	}
    }
}
