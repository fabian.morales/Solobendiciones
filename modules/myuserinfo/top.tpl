{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!-- Block user information module HEADER -->
<div class="row">
    <div class="col-md-offset-3 col-md-9">
        <div class="block_user_login">
            {if $logged}
                <a href="{$link->getPageLink('my-account', true)|escape:'html'}" title="{l s='View my customer account' mod='blockuserinfo'}" class="account" rel="nofollow"><span>{$cookie->customer_firstname} {$cookie->customer_lastname}</span></a> |
                <a href="{$link->getPageLink('index', true, NULL, "mylogout")|escape:'html'}" title="{l s='Log me out' mod='blockuserinfo'}" class="logout" rel="nofollow">{l s='Sign out' mod='blockuserinfo'}</a>
            {else}
                <a href="{$link->getPageLink('my-account', true)|escape:'html'}" title="{l s='Log in to your customer account' mod='blockuserinfo'}" class="login" rel="nofollow"><i class="icon icon-user"></i> Iniciar sesión | Regístrate</a>
            {/if}
        </div>
    </div>
</div>


<div class="row">
    <div class="col-sm-offset-1 col-sm-5 col-xs-12">
        <input class="buscador" type="text" placeholder="Busca tus productos" />
    </div>
    <div class="col-sm-offset-1 col-md-5 col-xs-12 text-center-small">
        <div class="block_user_carrito">
            <a href="{$link->getPageLink($order_process, true)|escape:'html'}" title="Ver mi carrito" rel="nofollow">
                <span> 
                    {if $cart_qties == 0} 
                        (0) Productos
                    {else} 
                        {if $cart_qties == 1} 
                            (1) Producto
                        {else} 
                            ({$cart_qties}) Productos
                        {/if}
                    {/if}
                </span>
            </a>
            <div class="imagen_carrito">
                <a href="{$link->getPageLink($order_process, true)|escape:'html'}" title="Ver mi carrito" rel="nofollow">
                    <img src="{$base_dir}themes/saludecco/imagenes/carrito.png" />
                </a>
            </div>
        </div>
    </div>
</div>




