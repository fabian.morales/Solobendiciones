(function ($, window) {
    $(document).ready(function () {
        $(window).resize(function() {                
            if ($("#despl-resp").is(":visible")){
                $("#menu_horz").hide();
            }
            else{                    
                $("#menu_horz").show();
            }
        });
        
        $('#slider_marcas').bxSlider({
            minSlides: 2,
            maxSlides: 3,
            slideMargin: 100,
            slideWidth: 200,
            pager: false
        });
        
        $('#slider_productos').bxSlider({
            minSlides: 2,
            maxSlides: 4,
            slideMargin: 100,
            slideWidth: 200,
            pager: false,
            controls: true
        });
        
        $('#banner_home').bxSlider({
            minSlides: 1,
            maxSlides: 1,
            slideMargin: 100,
            slideWidth: 3000,
            pager: false,
            controls: true
        });
        
        $("#despl-resp a").click(function(e) {
            e.preventDefault();
            $("#menu_horz").toggle("slow");
        });
        
        $("#lnkTiendas").fancybox({
            type: 'ajax',
            autoSize: false, 
            maxWidth: '700px'
        });
    });
})(jQuery, window);
