{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{include file="$tpl_dir./errors.tpl"}
{capture name=path}{$marca->name}{/capture}
<div class="row">
    <div class="col-md-2">
        <h1 class="titulo_cat">Marcas</h1>
        {if $marcas|@count > 0}
        <ul class="lista-cat">
            {foreach from=$marcas item=m}
            <li>
                <a href="{$base_dir}index.php?controller=custom&tarea=marcas&id_marca={$m.id_supplier}">
                    {$m.name|escape:'html':'UTF-8'}
                </a>
            </li>
            {/foreach}
        </ul>
        {/if}
    </div>
    <div class="col-md-10">
        {if isset($marca)}
            {if $marca->id AND $marca->active}
                {if $marca->description || $marca->image}
                    <div class="content_scene_cat">
                        <div>
                            <img class="img-responsive img-fullw" src="{$img_sup_dir}{$marca->id|escape:'html':'UTF-8'}-marca_std.jpg" />
                            <br />
                            {if $marca->description}
                                <div class="row">
                                    <div class="col-sm-3 col-md-2">
                                        <img class="img-responsive" src="{$base_dir}themes/saludecco/imagenes/logo_sobio.png" />
                                    </div>
                                    <div class="col-sm-9 col-md-10">
                                        <div class="cat_desc">
                                            <div class="rte">{$marca->description}</div>
                                        </div>
                                    </div>
                                </div>
                            {/if}
                        </div>
                    </div>
                {/if}

                {if $productos}
                    <div class="content_sortPagiBar clearfix">
                        <div class="top-pagination-content clearfix">
                            {*include file="./product-compare.tpl"*}
                            {include file="$tpl_dir./pagination.tpl"}
                        </div>
                    </div>
                    {include file="./product-list.tpl" products=$productos}
                    <div class="content_sortPagiBar">
                        <div class="bottom-pagination-content clearfix">
                            {include file="./pagination.tpl" paginationId='bottom'}
                        </div>
                    </div>
                {/if}
            {elseif $marca.id_supplier}
                <p class="alert alert-warning">Esta marca no est&aacute; disponible</p>
            {/if}
        {/if}        
    </div>
</div>

