<section id="faq">
    <div class="container">
        <!-- SECTION TITLE -->
        <div class="row">
            <div class="col-sm-12 titlebar">
                <h3>¿D&oacute;nde comprar?</h3>
                <p>Ubica tu punto de venta m&aacute;s cercano</p>
            </div>
        </div>

        <div class="row">
            <!-- Question #1 -->
            <div class="col-md-6 question animated" data-animation="fadeInUp" data-animation-delay="300">
                <h4><span class="faq-question-label">&nbsp;</span> Farmacia Vida Ideal</h4>
                <p>Av Cra 9 No. 126A-28 Local 1<br />
                    Tel.: 322-3125 / 311-220 0048<br />
                    Bogot&aacute;</p>
            </div>

            <!-- Question #2 -->
            <div class="col-md-6 question animated" data-animation="fadeInUp" data-animation-delay="300">
                <h4><span class="faq-question-label">&nbsp;</span> Unimed Am&eacute;ricas</h4>
                <p>Calle 8 #72A-31<br />
                    Tel.: 411-6602<br />
                    Bogot&aacute;</p>
            </div>
        </div>

        <div class="row">
            <!-- Question #3 -->
            <div class="col-md-6 question animated" data-animation="fadeInUp" data-animation-delay="600">						
                <h4><span class="faq-question-label">&nbsp;</span> Pepitas de salud</h4>
                <p>Centro Comercial Llano Centro Local 126<br />
                    Tel.: 668-2704<br />
                    Villavicencio</p>
            </div>

            <!-- Question #4 -->
            <div class="col-md-6 question animated" data-animation="fadeInUp" data-animation-delay="600">						
                <h4><span class="faq-question-label">&nbsp;</span> Cl&iacute;nica Sochagota</h4>
                <p>Calle 25 No 5-100<br />
                    Tel.: 310-764 8507<br />
                    Paipa</p>
            </div>
        </div>

        <div class="row">
            <!-- Question #5 -->
            <div class="col-md-6 question animated" data-animation="fadeInUp" data-animation-delay="900">						
                <h4><span class="faq-question-label">&nbsp;</span> Farmacia Medibiom</h4>
                <p>Calle 3ra No 2-76 Local 6<br />
                    Tel.: 824-4111<br />
                    Popay&aacute;n</p>
            </div>

            <!-- Question #6 -->	
            <div class="col-md-6 question animated" data-animation="fadeInUp" data-animation-delay="900">						
                <h4><span class="faq-question-label">&nbsp;</span> Farmacia Biol&oacute;gica del Sur</h4>
                <p>Centro Comercial Valle de Atriz Local 227<br />
                    Tel.: 731-2034<br />
                    Pasto</p>
            </div>
        </div>

        <div class="row">
            <!-- Question #6 -->	
            <div class="col-md-6 question animated" data-animation="fadeInUp" data-animation-delay="900">						
                <h4><span class="faq-question-label">&nbsp;</span> BioPharma Natural's</h4>
                <p>Carrera 51B Calle 106 ESQUINA Local 4<br />
                    Tel.: 385-4808 / 301-360 0884<br />
                    Barranquilla</p>
            </div>

            <!-- Question #5 -->
            <div class="col-md-6 question animated" data-animation="fadeInUp" data-animation-delay="900">						
                <h4><span class="faq-question-label">&nbsp;</span> BioPharma Natural's Villa Santos</h4>
                <p>Calle 3ra No 2-76 Local 6<br />
                    Tel.: 824-4111<br />
                    Barranquilla</p>
            </div>
        </div>

        <div class="row">
            <!-- Question #6 -->	
            <div class="col-md-6 question animated" data-animation="fadeInUp" data-animation-delay="900">						
                <h4><span class="faq-question-label">&nbsp;</span> Farmacia Jade</h4>
                <p>Carrera 76 # 32 C-27<br />
                    Tel.: 448 66 91<br />
                    Medell&iacute;n</p>
            </div>

            <!-- Question #5 -->
            <div class="col-md-6 question animated" data-animation="fadeInUp" data-animation-delay="900">						
                <h4><span class="faq-question-label">&nbsp;</span> Farmacia Jade</h4>
                <p>Transversal 35 C sur # 33-103 Barrio Los Naranjos<br />
                    Tel.: 2707311<br />
                    Envigado</p>
            </div>
        </div>

        <div class="row">
            <!-- Question #6 -->	
            <div class="col-md-6 question animated" data-animation="fadeInUp" data-animation-delay="900">						
                <h4><span class="faq-question-label">&nbsp;</span> La Farmacia Biol&oacute;gica</h4>
                <p>Local 104, Cl. 5d #39-25<br />
                   Tel.:(2) 3798843<br />
                   Cali
            </div>

            <!-- Question #5 -->
            <div class="col-md-6 question animated" data-animation="fadeInUp" data-animation-delay="900">						
                <h4><span class="faq-question-label">&nbsp;</span> Bandar Farmacia Homeop&aacute;tica</h4>
                <p>Av. Pradilla No 2-75 Local 103 <br />
                    Tels.: 885 8787 - 313 447 2138<br />
                    Ch&iacute;a </p>
            </div>
        </div>

        <div class="row">
            <!-- Question #6 -->
            <div class="col-md-6 question animated" data-animation="fadeInUp" data-animation-delay="900">						
                <h4><span class="faq-question-label">&nbsp;</span> La Farmacia Biol&oacute;gica Sur</h4>
                <p>Avenida Roosevelt  # 39-25 local 104<br />
                    Tels.: 3798843 / 5533934 <br />
                    Cali</p>
            </div>

            <!-- Question #5 -->
            <div class="col-md-6 question animated" data-animation="fadeInUp" data-animation-delay="900">						
                <h4><span class="faq-question-label">&nbsp;</span> La Farmacia Biol&oacute;gica Norte</h4>
                <p>Avenida 6 Bis norte # 35 N-15 local 105<br />
                    Tel.: 8965598<br />
                    Cali</p>
            </div>
        </div>

        <div class="row">
            <!-- Question #6 -->
            <div class="col-md-6 question animated" data-animation="fadeInUp" data-animation-delay="900">						
                <h4><span class="faq-question-label">&nbsp;</span> La Farmacia Biol&oacute;gica</h4>
                <p>Calle 32 # 24-74<br />
                    Tel.: 2859897<br />
                    Palmira</p>
            </div>

            <!-- Question #5 -->
            <div class="col-md-6 question animated" data-animation="fadeInUp" data-animation-delay="900">						
                <h4><span class="faq-question-label">&nbsp;</span> La Farmacia Biol&oacute;gica</h4>
                <p>Calle 25 # 35-38<br />
                    Tel.: 2343344<br />
                    Tulúa</p>
            </div>
        </div>

        <div class="row">
            <!-- Question #6 -->
            <div class="col-md-6 question animated" data-animation="fadeInUp" data-animation-delay="900">						
                <h4><span class="faq-question-label">&nbsp;</span> La Farmacia Biol&oacute;gica</h4>
                <p>Carrera 5 # 8-54<br />
                    Tel.: pr&oacute;ximamente<br />
                    Cartago</p>
            </div>

            <!-- Question #5 -->
            <div class="col-md-6 question animated" data-animation="fadeInUp" data-animation-delay="900">						
                &nbsp;
            </div>
        </div>
    </div>	    <!-- End container -->
</section>	 <!-- END FAQs -->